/**
 * HTTP Server example
 *
 * RC - 2012/2013 (LEI - FCT/UNL)
 *
 */

import java.io.*;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.plaf.SliderUI;
import javax.swing.text.html.HTMLDocument.HTMLReader.ParagraphAction;

public class Proxy {

	static final int PORT = 8080;

	/**
	 * MAIN - accept and handle client connections
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		ServerSocket ss = new ServerSocket(PORT);
		Map<String, Entry<String, byte[]>> cache = new HashMap<String, Entry<String, byte[]>>();

		for (;;) {
			System.out.println("Server ready at " + PORT);
			Socket clientS = ss.accept();

			Thread t = new ClientHTTPHandler(clientS, cache);
			t.start();
		}
	}
}

/**
 * Handler dos pedidos dos clientes.
 * 
 */
class ClientHTTPHandler extends Thread {
	Socket localSock;
	static Map<String, Entry<String, byte[]>> cache;

	public static final int DEFAULT_PORT = 80;

	/**
	 * initialize the new object to handle one client in a new thread
	 */
	public ClientHTTPHandler(Socket s, Map<String, Entry<String, byte[]>> cache) {
		localSock = s;
		this.cache = cache;
	}

	/**
	 * processHTTPrequest - handle one HTTP request
	 * 
	 * @param in
	 *            - stream from client
	 * @param out
	 *            - stream to client
	 */
	private static void processHTTPrequest(InputStream in, OutputStream out) {
		try {

			String request = HTTPUtilities.readLine(in);

			String[] requestParts = HTTPUtilities.parseHttpRequest(request);

			// parse url
			URL u = new URL(requestParts[1]);

			// In case of a non HTTP request
			if (!u.getProtocol().equalsIgnoreCase("HTTP"))
				throw new MalformedURLException();

			// Handling the client request
			if (requestParts[0].equalsIgnoreCase("GET")) {

				processGETrequest(u.getHost(), u.getPort(), u.getFile(), in,
						out);

			} else if (requestParts[0].equalsIgnoreCase("POST")) {
				processPOSTrequest(u.getHost(), u.getPort(), u.getFile(), out,
						in);
			} else if (requestParts[0].equalsIgnoreCase("PUT")) {
				processPOSTrequest(u.getHost(), u.getPort(), u.getFile(), out,
						in);
			} else { // ignore other requests
				dumpStream(errorMessageStream(), out);
			}

		} catch (MalformedURLException e) {
			System.out
					.println("The request has syntax error or the URL is marlformed.");
		} catch (IOException e) {
			System.err.println(e.getMessage());

		}
	}

	/**
	 * Processes the POST request
	 * 
	 * @param host
	 *            server name
	 * @param port
	 *            TCP port
	 * @param file
	 *            name of the page
	 * @param clientOutput
	 *            output channel to the client
	 * @param clientInput
	 *            input channel to the client
	 */
	static void processPOSTrequest(String host, int port, String file,
			OutputStream clientOutput, InputStream clientInput) {
		try {
			// In case of ommited port. Use default http port is 80
			if (port == -1)
				port = 80;
			// In case of an empty path
			if (file.equals(""))
				file = "/";

			Socket serverSocket = new Socket(host, port);

			OutputStream serverOutput = serverSocket.getOutputStream();

			StringBuilder requestToServer = new StringBuilder("POST " + file
					+ " HTTP/1.0\r\n");

			String line = HTTPUtilities.readLine(clientInput);
			String[] str;
			int contentLength = 0;

			// reads the headers of the request
			while (!line.equalsIgnoreCase("")) {

				str = HTTPUtilities.parseHttpHeader(line);
				// Filters ONLY the http/1.0 headers
				if (str[0].equalsIgnoreCase("Host")
						|| str[0].equalsIgnoreCase("Date")
						|| str[0].equalsIgnoreCase("Pragma")
						|| str[0].equalsIgnoreCase("Authorization")
						|| str[0].equalsIgnoreCase("From")
						|| str[0].equalsIgnoreCase("If-Modified-Since")
						|| str[0].equalsIgnoreCase("Referer")
						|| str[0].equalsIgnoreCase("User-Agent")
						|| str[0].equalsIgnoreCase("Allow")
						|| str[0].equalsIgnoreCase("Content-Encoding")
						|| str[0].equalsIgnoreCase("Content-Type")
						|| str[0].equalsIgnoreCase("Content-Length")
						|| str[0].equalsIgnoreCase("Expires")
						|| str[0].equalsIgnoreCase("Last-Modified"))
					requestToServer.append(line + "\r\n");

				// Reads the content-length
				if (str[0].equalsIgnoreCase("Content-Length"))
					contentLength = Integer.parseInt(str[1]);

				line = HTTPUtilities.readLine(clientInput);
			}
			// CRLF
			requestToServer.append("\r\n");

			// reads the BODY of the request
			for (int i = 0; i < contentLength; i++) {
				requestToServer.append((char) clientInput.read());
			}

			// sends the post message to the server
			dumpStream(new ByteArrayInputStream(requestToServer.toString()
					.getBytes()), serverOutput);
			serverOutput.flush();

			// server response
			InputStream in = serverSocket.getInputStream();
			dumpStream(in, clientOutput);
			clientOutput.flush();

		} catch (UnknownHostException e) {
			System.out.println("unknown");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Processes the GET request to the server using HTTP 1.0
	 * 
	 * @param host
	 *            server name
	 * @param file
	 *            page to get
	 */
	static void processGETrequest(String host, int port, String file,
			InputStream clientInput, OutputStream clientOutput) {
		try {

			// In case of ommited port. Use default http port is 80
			if (port == -1)
				port = DEFAULT_PORT;
			// In case of an empty path
			if (file.equals(""))
				file = "/";

			Socket serverSocket = new Socket(host, port);
			OutputStream serverOutput = serverSocket.getOutputStream();
			InputStream serverInput = serverSocket.getInputStream();

			StringBuilder requestToServer = new StringBuilder("GET " + file
					+ " HTTP/1.0\r\n");

			String line = HTTPUtilities.readLine(clientInput);
			String[] str;
			while (!line.equalsIgnoreCase("")) {
				str = HTTPUtilities.parseHttpHeader(line);
				// Filters ONLY the http/1.0 headers
				if (str[0].equalsIgnoreCase("Host")
						|| str[0].equalsIgnoreCase("Date")
						|| str[0].equalsIgnoreCase("Pragma")
						|| str[0].equalsIgnoreCase("Authorization")
						|| str[0].equalsIgnoreCase("From")
						|| str[0].equalsIgnoreCase("If-Modified-Since")
						|| str[0].equalsIgnoreCase("Referer")
						|| str[0].equalsIgnoreCase("User-Agent")
						|| str[0].equalsIgnoreCase("Allow")
						|| str[0].equalsIgnoreCase("Content-Encoding")
						|| str[0].equalsIgnoreCase("Content-Type")
						|| str[0].equalsIgnoreCase("Content-Length")
						|| str[0].equalsIgnoreCase("Expires")
						|| str[0].equalsIgnoreCase("Last-Modified"))
					requestToServer.append(line + "\r\n");

				// reads the next line of the request
				line = HTTPUtilities.readLine(clientInput);
			}
			requestToServer.append("\r\n");

			// sends the post message to the server
			dumpStream(new ByteArrayInputStream(requestToServer.toString()
					.getBytes()), serverOutput);
			serverOutput.flush();

			// server response
			dumpStream(serverInput, clientOutput);
			clientOutput.flush();

		} catch (UnknownHostException e) {
			System.out.println("unknown");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Copia o conteudo do stream in para o stream out
	 */
	static void dumpStream(InputStream in, OutputStream out) throws IOException {
		byte[] arr = new byte[1024];
		for (;;) {
			int n = in.read(arr);
			if (n == -1)
				break;
			out.write(arr, 0, n);
		}
	}

	/**
	 * Devolve um input stream com o conteudo dos headers e HTML que pode ser
	 * usado como resposta em caso de erro.
	 */
	static InputStream errorMessageStream() {
		final String errorPage = "<HTML><BODY>Request Not Supported</BODY></HTML>";

		StringBuilder reply = new StringBuilder(
				"HTTP/1.0 501 Not Implemented\r\n");

		int length = errorPage.length();
		reply.append("Content-Length: " + String.valueOf(length) + "\r\n\r\n");
		reply.append(errorPage);

		return new ByteArrayInputStream(reply.toString().getBytes());
	}

	/**
	 * start method for each new server thread handle one client
	 */
	public void run() {
		try {

			InputStream in = localSock.getInputStream();
			OutputStream out = localSock.getOutputStream();

			processHTTPrequest(in, out);

			localSock.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

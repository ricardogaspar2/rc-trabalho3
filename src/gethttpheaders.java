// get the HTTP headers 

import java.net.*;
import java.io.*;

public class gethttpheaders
{
    public static void main ( String[] args ) throws IOException 
    {
    try 
	{    
	    URL url = new URL( args[0] );

	    URLConnection c = url.openConnection();
    
	    for (int i=0; ; i++) 
		{
		    String name = c.getHeaderFieldKey(i);
		    String value = c.getHeaderField(i);
    
		    if (name == null && value == null)     // end of headers
			{
			    break;         
			}

		    if (name == null)     // first line of headers
			{
			    System.out.println("Server HTTP version, Response code:");
			    System.out.println(value);
			    System.out.print("\n");
			}
		    else
			{
			    System.out.println(name + "=" + value);
			}
		}
	} 
    catch (Exception e) {}
    }
}
